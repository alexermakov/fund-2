function onLoad(callback){
    if (document.readyState === 'complete') {
        callback();
    } else {
        window.addEventListener("load", callback);
    }
}


let animateStartCount = false;





$(function () {
	onLoad(function(){
		$('body').addClass('loaded')
	});

	$('.js_fixed__call').click(function(e){
		if ($(window).width()>550){
			e.preventDefault();
			Fancybox.show([{src: "#js_modal_call"}]);
		}
	})

	$.fn.isInViewport = function () {
		let elementTop = $(this).offset().top;
		let elementBottom = elementTop + $(this).outerHeight();

		let viewportTop = $(window).scrollTop();
		let viewportBottom = viewportTop + $(window).height();

		return elementBottom > viewportTop && elementTop < viewportBottom;
	};

	$(window).on('resize scroll', function () {
		if ($('.js_count_animate').isInViewport() && !animateStartCount) {
			animateStartCount = true;
			$('.js_count_animate').each(function () {
				var $this = $(this),
					countTo = $this.attr('data-number');
				$({
					countNum: $this.text()
				}).animate({
						countNum: countTo
					},
					{
						duration: 3500,
						easing: 'linear',
						step: function () {
							$this.text(Math.floor(this.countNum));
						},
						complete: function () {
							$this.text(this.countNum);
						}
					});
			});
		}
	});

	Fancybox.bind('[data-fancybox]', {})

	Fancybox.bind('.js__modal', {
		autoFocus: false,
		trapFocus: false,
		touch: false,
		closeButton: 'outside',
	})

	Fancybox.defaults.Thumbs = false

	$('.js_select').each(function () {
		let placeholder = $(this).attr('placeholder')
		$(this).select2({
			minimumResultsForSearch: 1 / 0,
			placeholder: placeholder,
			// allowClear: true
		})
	})

	$('.js_home_review').slick({
		centerMode: true,
		centerPadding: '14rem',
		slidesToShow: 2,

		appendArrows:$('.js_home_review__slider__block__arrow'),
		nextArrow:'<button type="button" class="home_review__slider__arrow home_review__slider__arrow--next"><svg width="18" height="8" viewBox="0 0 18 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 3.5C0.723858 3.5 0.5 3.72386 0.5 4C0.5 4.27614 0.723858 4.5 1 4.5V3.5ZM17.3536 4.35355C17.5488 4.15829 17.5488 3.84171 17.3536 3.64645L14.1716 0.464466C13.9763 0.269204 13.6597 0.269204 13.4645 0.464466C13.2692 0.659728 13.2692 0.976311 13.4645 1.17157L16.2929 4L13.4645 6.82843C13.2692 7.02369 13.2692 7.34027 13.4645 7.53553C13.6597 7.7308 13.9763 7.7308 14.1716 7.53553L17.3536 4.35355ZM1 4.5H17V3.5H1V4.5Z" fill="#222222"/></svg></button>',
		prevArrow:'<button type="button" class="home_review__slider__arrow home_review__slider__arrow--prev"><svg width="18" height="8" viewBox="0 0 18 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.646447 3.64645C0.451184 3.84171 0.451184 4.15829 0.646447 4.35355L3.82843 7.53553C4.02369 7.7308 4.34027 7.7308 4.53553 7.53553C4.7308 7.34027 4.7308 7.02369 4.53553 6.82843L1.70711 4L4.53553 1.17157C4.7308 0.976311 4.7308 0.659728 4.53553 0.464466C4.34027 0.269204 4.02369 0.269204 3.82843 0.464466L0.646447 3.64645ZM17 4.5C17.2761 4.5 17.5 4.27614 17.5 4C17.5 3.72386 17.2761 3.5 17 3.5V4.5ZM1 4.5H17V3.5H1V4.5Z" fill="#222222"/></svg></button>',
		responsive: [
			{
				breakpoint: 951,
				settings: {
					centerPadding: '10rem',
					slidesToShow: 1,
				},
			},
			{
				breakpoint: 551,
				settings: {
					centerPadding: '35px',
					slidesToShow: 1,
				},
			},
		],
	})

	$('.js__btn_close__modal').click(function (e) {
		Fancybox.close()
	})

	$('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(
		function (e) {
			e.preventDefault()
			$('.js_btn_menu').toggleClass('active')
			$('.js__modal__menu').toggleClass('active')
			$('.js_modal__menu_overlay').toggleClass('active')
		}
	)

	if (localStorage.hasOwnProperty('sendedForm')) {
		$('.js_btn_form_modal_form').prop("disabled", true)
	}


	$('.js_form').submit(function (event) {
        if ($(this)[0].checkValidity()) {
            let form = $(this);
            let button = $(this).find('button');
            button.prop('disabled',true)
            // $.ajax({
            //     type: "POST",
            //     url: ajax_url,
            //     data: formData,
            //     success: function () {
					localStorage.setItem('sendedForm', true)
                    form.trigger("reset");
                    button.prop('disabled',false)
                    Fancybox.close();
                    Fancybox.show([{src: "#js_modal_thanks"}]);
                // }
            // });
        }
    });

})
