<?php $version = '0.0.7';?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">



<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png?1">
<meta name="msapplication-TileColor" content="#876B3E">
<meta name="theme-color" content="#ffffff">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lexend+Zetta:wght@100..900&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">



<link rel="stylesheet" href="css/vendor/select2.min.css">
<link rel="stylesheet" href="css/vendor/slick.css">
<link rel="stylesheet" href="css/vendor/fancybox.css">
<link rel="stylesheet" href="css/app.min.css?<?= $version;?>">