<div class="js__modal__menu modal__menu">
    <div class="modal__menu__inner">

        <div class="modal__menu__top">
            <a href="index.php" class="modal__menu__logo">
                <img src="images/logo.svg" alt="main logo">
            </a>

            <button class="modal__menu__btn_close btn__close js_modal__menu__btn_close"></button>
        </div>


        <div class="modal__menu__list js_modal__menu__list">
            <ul>
                <li><a href="">About Us</a></li>
                <li class='has_submenu'>
                    <a href="">Resources</a>
                    <ul class="sub-menu">
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                    </ul>
                </li>
                <li><a href="">Portfolio</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">Contact Us</a></li>
            </ul>
        </div>

        <div class="modal__menu__info">
            <a href="tel:646-462-6380" class="modal__menu__phone">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.4492 10.8479C9.4492 10.8479 10.5739 10.204 10.8684 10.0493C11.162 9.89397 11.4661 9.8548 11.6509 9.96758C11.9306 10.1387 14.2795 11.7012 14.4772 11.8396C14.675 11.9782 14.7703 12.374 14.4984 12.7618C14.2275 13.1496 12.9781 14.6829 12.4486 14.6665C11.9184 14.6493 9.71364 14.601 5.55616 10.4423C1.3995 6.28502 1.35034 4.07971 1.33339 3.54942C1.31645 3.01942 2.84978 1.76967 3.23755 1.49856C3.62588 1.22772 4.02227 1.32967 4.16005 1.52022C4.31672 1.73717 5.86116 4.0786 6.03116 4.34694C6.14754 4.53027 6.10477 4.83611 5.94949 5.13001C5.79504 5.42445 5.15116 6.54919 5.15116 6.54919C5.15116 6.54919 5.60505 7.32337 7.14004 8.85812C8.67531 10.3931 9.4492 10.8479 9.4492 10.8479Z" stroke="white" stroke-miterlimit="10"/>
                </svg>
                <span>646-462-6380</span>
            </a>
            <a href="javascript:void(0)" data-fancybox data-src='#js_modal_call' class="btn_default btn_ghost btn_modal__menu_login">Investors login</a>
        </div>

    </div>
</div>


<div class="modal_default modal_call" id='js_modal_call'>
    <div class="modal_default__inner">
        <div class="modal_default__title">Request call</div>
        <div class="modal_default__form">
            <form action="javascript:void(0)" class="js_form">
                <div class="field_x__group">
                    <div class="field_x__row">
                        <label class="field_x__item">
                            <div class="field_x__item_title">Name</div>
                            <div class="field_x__item_value">
                                <input type="text" name="name" placeholder="Name" required>
                            </div>
                        </label>
                    </div>

                    <div class="field_x__row">
                        <label class="field_x__item">
                            <div class="field_x__item_title">Phone</div>
                            <div class="field_x__item_value">
                                <input type="phone" name="phone" placeholder="Phone" required>
                            </div>
                        </label>
                    </div>

                    <div class="field_x__row">
                        <div class="field_x__item field_x__item--radio">
                            <div class="field_x__item_title">Select department</div>
                            <div class="field_x__item_value field_x__item_value--radio">
                                <label class="field_x__item_value--radio_item">
                                    <input type='radio' name='department' required>
                                    <div class="field_x__item_value--radio_item__view"></div>
                                    <div class="field_x__item_value--radio_item__title">Sales</div>
                                </label>
                                <label class="field_x__item_value--radio_item">
                                    <input type='radio' name='department' required>
                                    <div class="field_x__item_value--radio_item__view"></div>
                                    <div class="field_x__item_value--radio_item__title">Customer Service</div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="field_x__row">
                        <div class="field_x__item">
                            <button class="btn_default btn_gold btn_form_modal_form js_btn_form_modal_form">Request call</button>
                        </div>
                    </div>
                    <div class="field_x__row">
                        <div class="field_x__row__add_text">Customer service representative will call you in 2-3 minutes</div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal_default modal_thanks" id='js_modal_thanks'>
    <div class="modal_default__inner modal_default__inner--thanks">
        <div class="modal_thanks__icon">
            <img src="images/icons/social/thanks.svg" alt="thanks">
        </div>
        <div class="modal_default__title modal_default__title--thanks">Thank you!</div>
        <div class="modal_default__text modal_default__text--thanks">We've got your request and will call you in a few minutes.</div>
    </div>
</div>
