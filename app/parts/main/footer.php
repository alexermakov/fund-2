<!-- start footer -->

<a href="tel:646-462-6380" class="fixed__call js_fixed__call">
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M22.4167 17.125V20.25C22.4179 20.5401 22.3585 20.8273 22.2423 21.0931C22.126 21.3589 21.9556 21.5975 21.7418 21.7936C21.528 21.9898 21.2757 22.1391 21.0009 22.232C20.726 22.3249 20.4348 22.3595 20.1459 22.3334C16.9405 21.9851 13.8615 20.8898 11.1563 19.1354C8.63948 17.5361 6.50563 15.4023 4.90632 12.8854C3.14588 10.1679 2.05033 7.07397 1.70841 3.85419C1.68238 3.56613 1.71661 3.27581 1.80893 3.00171C1.90125 2.72761 2.04963 2.47574 2.24462 2.26212C2.43962 2.04851 2.67695 1.87784 2.94152 1.76098C3.20609 1.64412 3.49209 1.58363 3.78132 1.58335H6.90632C7.41185 1.57838 7.90194 1.75739 8.28524 2.08703C8.66854 2.41667 8.9189 2.87444 8.98966 3.37502C9.12155 4.37509 9.36617 5.35703 9.71882 6.3021C9.85897 6.67494 9.8893 7.08014 9.80623 7.46969C9.72315 7.85923 9.53014 8.2168 9.25007 8.50002L7.92716 9.82294C9.41003 12.4308 11.5693 14.5901 14.1772 16.0729L15.5001 14.75C15.7833 14.47 16.1409 14.2769 16.5304 14.1939C16.92 14.1108 17.3252 14.1411 17.698 14.2813C18.6431 14.6339 19.625 14.8785 20.6251 15.0104C21.1311 15.0818 21.5932 15.3367 21.9235 15.7266C22.2539 16.1165 22.4294 16.6142 22.4167 17.125Z" stroke="white" stroke-width="2.08333" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
</a>


<footer class="main_footer">
    <div class="container">
        <div class="main_footer__content">

            <div class="main_footer__contact __animate__top">
                <a href="tel:+1 (347) 263-3592" class="main_footer__phone">+1 (347) 263-3592</a>
                <a href="mailto:info@lendvent.com" class="main_footer__mail">info@lendvent.com</a>
                <div class="main_footer__logo">
                    <img src="images/logo.svg" alt="main logo">
                </div>
                <div class="main_footer__adress">
                    NEW YORK OFFICE <br>
                    9 Park Place, 1st Floor<br>
                    Greatr Neck Plaza, NY 11021
                </div>
                <div class="main_footer__social">
                    <a href="" class="main_footer__social__link" target='_blank'>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17 2H7C4.23858 2 2 4.23858 2 7V17C2 19.7614 4.23858 22 7 22H17C19.7614 22 22 19.7614 22 17V7C22 4.23858 19.7614 2 17 2Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M16 11.3698C16.1234 12.2021 15.9812 13.052 15.5937 13.7988C15.2062 14.5456 14.5931 15.1512 13.8416 15.5295C13.0901 15.9077 12.2384 16.0394 11.4077 15.9057C10.5771 15.7721 9.80971 15.3799 9.21479 14.785C8.61987 14.1901 8.22768 13.4227 8.09402 12.592C7.96035 11.7614 8.09202 10.9097 8.47028 10.1582C8.84854 9.40667 9.45414 8.79355 10.2009 8.40605C10.9477 8.01856 11.7977 7.8764 12.63 7.99981C13.4789 8.1257 14.2648 8.52128 14.8716 9.12812C15.4785 9.73496 15.8741 10.5209 16 11.3698Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M17.5 6.5H17.51" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
                    <a href="" class="main_footer__social__link" target='_blank'>
                        <svg width="13" height="22" viewBox="0 0 13 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 1H9C7.67392 1 6.40215 1.52678 5.46447 2.46447C4.52678 3.40215 4 4.67392 4 6V9H1V13H4V21H8V13H11L12 9H8V6C8 5.73478 8.10536 5.48043 8.29289 5.29289C8.48043 5.10536 8.73478 5 9 5H12V1Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="main_footer__form __animate__opacity" data-delay='0.25'>
                <div class="main_footer__form__content">
                    <div class="main_footer__form__title">Contact us</div>
                    <div class="main_footer__form__x">
                        <form action="" class="js_form">
                            <div class="field_x__group">
                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Full name</div>
                                        <div class="field_x__item_value">
                                            <input type="text" name="fullname" placeholder="Full name" required>
                                        </div>
                                    </label>
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Company name</div>
                                        <div class="field_x__item_value">
                                            <input type="text" name="companyname" placeholder="Company name" required>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Email</div>
                                        <div class="field_x__item_value">
                                            <input type="email" name="email" placeholder="Email" required>
                                        </div>
                                    </label>
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Phone</div>
                                        <div class="field_x__item_value">
                                            <input type="phone" name="phone" placeholder="Phone" required>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Message</div>
                                        <div class="field_x__item_value">
                                            <textarea name="message" placeholder="Enter here"></textarea>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <div class="field_x__item">
                                        <button class="btn_default btn_gold btn_form_hero">Send</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="main_footer__bottom __animate__top">
                <div class="main_footer__bottom__likns">
                    <a href="">Privacy Policy</a>
                    <a href="">Terms of Use</a>
                </div>
                <div class="main_footer__copyright">Copyright © 2024</div>
            </div>
        </div>
    </div>
</footer>


<script src="js/vendor/jquery.min.js"></script>
<script src="js/vendor/fancybox.umd.js"></script>
<script src="js/vendor/slick.min.js"></script>
<script src="js/vendor/gsap.min.js"></script>
<script src="js/vendor/ScrollTrigger.min.js"></script>
<script src="js/vendor/jquery.inputmask.bundle.js"></script>
<script src="js/vendor/select2.min.js"></script>
<script src="js/main.js?<?= $version;?>"></script>
<script src="js/animate.js?<?= $version;?>"></script>