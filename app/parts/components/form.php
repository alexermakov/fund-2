<div class="contact__block__form__block __animate__top">
    <div class="contact__block__form__content js_contact__block_form">

        <div class="contact__block__form__content__title title_y">Book an appointment</div>
        <div class="contact__block__form">
            <form action="javascript:void(0)" class="js_form_contact">
                <div class="field_group">
                    <div class="field_row">
                        <label class="field_item">
                            <div class="field_item__title">Contact name</div>
                            <div class="field_item__content">
                                <input type="text" name="name" required placeholder="Full name">
                            </div>
                        </label>
                        <label class="field_item">
                            <div class="field_item__title">Phone</div>
                            <div class="field_item__content">
                                <input type="tel" name="phone" required placeholder="Phone">
                            </div>
                        </label>
                    </div>

                    <div class="field_row">
                        <label class="field_item">
                            <div class="field_item__title">Date</div>
                            <div class="field_item__content">
                                <input type="date" name="date" class="js_field_date" placeholder="__/__/____" required value="<?= date('m/d/Y');?>">
                            </div>
                        </label>
                        <label class="field_item">
                            <div class="field_item__title">Time</div>
                            <div class="field_item__content">
                                <input type="time" name="time" class="js_field_time" placeholder="10:00 am" required value="12:00">
                            </div>
                        </label>
                    </div>

                    <div class="field_row">
                        <label class="field_item">
                            <div class="field_item__title">Message</div>
                            <div class="field_item__content">
                            <textarea name="message"placeholder="What would you like to sell etc."></textarea>
                            </div>
                        </label>
                    </div>

                    <div class="field_row">
                        <div class="field_item field_item--file js_field_item--file">
                            <label class="field_item--file__value">
                               <input type="file" name="files[]" multiple>
                                <div class="field_item--file__title">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M21.4398 11.0499L12.2498 20.2399C11.124 21.3658 9.59699 21.9983 8.0048 21.9983C6.41262 21.9983 4.88565 21.3658 3.7598 20.2399C2.63396 19.1141 2.00146 17.5871 2.00146 15.9949C2.00146 14.4027 2.63396 12.8758 3.7598 11.7499L12.9498 2.55992C13.7004 1.80936 14.7183 1.3877 15.7798 1.3877C16.8413 1.3877 17.8592 1.80936 18.6098 2.55992C19.3604 3.31048 19.782 4.32846 19.782 5.38992C19.782 6.45138 19.3604 7.46936 18.6098 8.21992L9.4098 17.4099C9.03452 17.7852 8.52553 17.996 7.9948 17.996C7.46407 17.996 6.95508 17.7852 6.5798 17.4099C6.20452 17.0346 5.99369 16.5256 5.99369 15.9949C5.99369 15.4642 6.20452 14.9552 6.5798 14.5799L15.0698 6.09992" stroke="#2B2420" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                    <span>Attach files</span>
                                </div>
                            </label>
                            <div class="field_item--file__list"></div>
                        </div>
                    </div>


                    <div class="g-recaptcha" data-sitekey="6Lenap4pAAAAALlpmZezqhZFUmGQ9k10GhOZ2wO9"></div>
                    <!-- 6Lda_ZkpAAAAAA8KAvWqj4Kxa6vnJWWmXZ0egcPP -->



                    <div class="field_row">
                        <div class="field_item">
                            <button class="btn_default btn_dark_yellow btn_form_send">Book an appointment now</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>

    </div>

    <div class="js_contact__block_success contact__block_success">
        <div class="contact__block_success__content">
            <div class="title_y-contact__block_success title_y">Thank you!</div>
            <div class="contact__block_success__text">
                <p>Your request has been send successfully. A member of our team will contact you. </p>
            </div>
            <div class="contact__block_success__contact">
                <div class="contact__block_success__contact__item">
                    <div class="contact__block_success__contact__item__icon">
                        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13.1667 2.66675H3.83333C3.09695 2.66675 2.5 3.2637 2.5 4.00008V13.3334C2.5 14.0698 3.09695 14.6667 3.83333 14.6667H13.1667C13.903 14.6667 14.5 14.0698 14.5 13.3334V4.00008C14.5 3.2637 13.903 2.66675 13.1667 2.66675Z" stroke="#876B3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M11.1665 1.33325V3.99992" stroke="#876B3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5.8335 1.33325V3.99992" stroke="#876B3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M2.5 6.66675H14.5" stroke="#876B3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                    <div class="contact__block_success__contact__item__value js_contact__block_success__contact__item__value--date"></div>
                </div>

                <div class="contact__block_success__contact__item">
                    <div class="contact__block_success__contact__item__icon">
                        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.50016 14.6666C12.1821 14.6666 15.1668 11.6818 15.1668 7.99992C15.1668 4.31802 12.1821 1.33325 8.50016 1.33325C4.81826 1.33325 1.8335 4.31802 1.8335 7.99992C1.8335 11.6818 4.81826 14.6666 8.50016 14.6666Z" stroke="#876B3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M8.5 4V8L11.1667 9.33333" stroke="#876B3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                    <div class="contact__block_success__contact__item__value js_contact__block_success__contact__item__value--time"></div>
                </div>
            </div>
        </div>
    </div>
</div>