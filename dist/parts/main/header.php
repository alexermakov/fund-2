
<header class="header_main">
    <div class="container">

        <a href="index.php" class="header__logo">
            <img src="images/logo.svg" alt="main logo">
        </a>

        <!-- <div class="header_menu">
            <ul>
                <li><a href="">About Us</a></li>
                <li class='has_submenu'>
                    <a href="">Resources</a>
                    <ul class="sub-menu">
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                        <li><a href="">Resources sub item</a></li>
                    </ul>
                </li>
                <li><a href="">Portfolio</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">Contact Us</a></li>
            </ul>
        </div> -->

        <div class="header__info">
            <a href="tel:646-462-6380" class="header__phone">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.4492 10.8479C9.4492 10.8479 10.5739 10.204 10.8684 10.0493C11.162 9.89397 11.4661 9.8548 11.6509 9.96758C11.9306 10.1387 14.2795 11.7012 14.4772 11.8396C14.675 11.9782 14.7703 12.374 14.4984 12.7618C14.2275 13.1496 12.9781 14.6829 12.4486 14.6665C11.9184 14.6493 9.71364 14.601 5.55616 10.4423C1.3995 6.28502 1.35034 4.07971 1.33339 3.54942C1.31645 3.01942 2.84978 1.76967 3.23755 1.49856C3.62588 1.22772 4.02227 1.32967 4.16005 1.52022C4.31672 1.73717 5.86116 4.0786 6.03116 4.34694C6.14754 4.53027 6.10477 4.83611 5.94949 5.13001C5.79504 5.42445 5.15116 6.54919 5.15116 6.54919C5.15116 6.54919 5.60505 7.32337 7.14004 8.85812C8.67531 10.3931 9.4492 10.8479 9.4492 10.8479Z" stroke="white" stroke-miterlimit="10"/>
                </svg>
                <span>646-462-6380</span>
            </a>
            <!-- <a href="javascript:void(0)" data-fancybox data-src='#js_modal_call' class="btn_default btn_ghost btn_header_login">Investors login</a> -->
            <!-- <button class="header_btn_menu btn_menu js_btn_menu">
                <span></span>
                <span></span>
                <span></span>
            </button> -->
        </div>

    </div>
</header>

<!-- end header -->
