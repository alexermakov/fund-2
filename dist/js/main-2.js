function onLoad(callback){
    if (document.readyState === 'complete') {
        callback();
    } else {
        window.addEventListener("load", callback);
    }
}


let animateStartCount = false;





$(function () {
	onLoad(function(){
		$('body').addClass('loaded')
	});

	$('.js_fixed__call').click(function(e){
		if ($(window).width()>550){
			e.preventDefault();
			Fancybox.show([{src: "#js_modal_call"}]);
		}
	})

	$.fn.isInViewport = function () {
		let elementTop = $(this).offset().top;
		let elementBottom = elementTop + $(this).outerHeight();

		let viewportTop = $(window).scrollTop();
		let viewportBottom = viewportTop + $(window).height();

		return elementBottom > viewportTop && elementTop < viewportBottom;
	};

	$(window).on('resize scroll', function () {
		if ($('.js_count_animate').isInViewport() && !animateStartCount) {
			animateStartCount = true;
			$('.js_count_animate').each(function () {
				var $this = $(this),
					countTo = $this.attr('data-number');
				$({
					countNum: $this.text()
				}).animate({
						countNum: countTo
					},
					{
						duration: 3500,
						easing: 'linear',
						step: function () {
							$this.text(Math.floor(this.countNum));
						},
						complete: function () {
							$this.text(this.countNum);
						}
					});
			});
		}
	});

	Fancybox.bind('[data-fancybox]', {})

	Fancybox.bind('.js__modal', {
		autoFocus: false,
		trapFocus: false,
		touch: false,
		closeButton: 'outside',
	})

	Fancybox.defaults.Thumbs = false

	$('.js_select').each(function () {
		let placeholder = $(this).attr('placeholder')
		$(this).select2({
			minimumResultsForSearch: 1 / 0,
			placeholder: placeholder,
			// allowClear: true
		})
	})


	$('.js__btn_close__modal').click(function (e) {
		Fancybox.close()
	})

	$('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(
		function (e) {
			e.preventDefault()
			$('.js_btn_menu').toggleClass('active')
			$('.js__modal__menu').toggleClass('active')
			$('.js_modal__menu_overlay').toggleClass('active')
		}
	)

	if (localStorage.hasOwnProperty('sendedForm')) {
		$('.js_btn_form_modal_form').prop("disabled", true)
	}

    $('.js_home_question__item .home_question__item__top').click(function (e) {
        e.preventDefault();
        const $el = $(this).closest('.js_home_question__item')
        $el.find('.home_question__item__content').slideToggle(500, function () {
            $el.toggleClass('active')
        });
    });


    function isInView(elem){
        return (elem.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ((elem.offset().top + elem.outerHeight()) -  $(window).height() / 2> ($(window).scrollTop()));
    }
    function isInViewStep(elem){
        let centerScreen = $(window).scrollTop() + $(window).height() / 2;
        return (elem.offset().top < centerScreen  - $(window).height() / 10) && (elem.offset().top + elem.outerHeight(true) > centerScreen + $(window).height() / 10);
    }





    if ($('.js_home_sliding__images').length){

        Stickyfill.forceSticky()
        let elements = $('.js_home_sliding__images');
        Stickyfill.add(elements);
        let indexHomeSliding = 0;

        function homeSliding(){
            if ($('.js_home_sliding').length){
                const $sliding            =  $('.js_home_sliding')
                const $slidingList        =  $sliding.find('.home_sliding__list')
                const $slidingSlides      =  $sliding.find('.home_sliding__slide')
                const $slidingSlideActive =  $sliding.find('.home_sliding__slide.active')
                const $slidingImagesList  =  $sliding.find('.home_sliding__images img')
                const $slidingImageActive =  $sliding.find('.home_sliding__images img.active')


                if (isInView($slidingList)){
                    $slidingSlides.each(function (index, element) {
                        if (isInViewStep($(this))) {
                            indexHomeSliding = index
                        }
                    });


                    $slidingImageActive.removeClass('active')
                    $slidingImagesList.eq(indexHomeSliding).addClass('active')


                    $slidingSlideActive.removeClass('active')
                    $slidingSlides.eq(indexHomeSliding).addClass('active')

                        // $('.js_home_counts__col_1__text__item.active').removeClass('active')
                        // $('.js_home_counts__col_1__text__item').eq(indexStep).addClass('active')

                        // $('.js_home_counts').css('background',$('.js_home_counts__step_x').eq(indexStep).data('bg'))


                }
            }
        }
        $(window).scroll(function(){
            homeSliding()

        })
    }

	$('.js_form').submit(function (event) {
        if ($(this)[0].checkValidity()) {
            let form = $(this);
            let button = $(this).find('button');
            button.prop('disabled',true)
            // $.ajax({
            //     type: "POST",
            //     url: ajax_url,
            //     data: formData,
            //     success: function () {
					localStorage.setItem('sendedForm', true)
                    form.trigger("reset");
                    button.prop('disabled',false)
                    Fancybox.close();
                    Fancybox.show([{src: "#js_modal_thanks"}]);
                // }
            // });
        }
    });

})
