function onLoad(callback){
    if (document.readyState === 'complete') {
        callback();
    } else {
        window.addEventListener("load", callback);
    }
}

$(document).ready(function () {

	function promo__title_animate(){
		let timeStep = 450;
		let timeMove = 3050;
		let $title_content = $('.js_home_promo__title__mask__content')
		let $title_active =  $title_content.find('span.active')
		let $title_new_active =  $title_content.find('span.active').next()


		setTimeout(() => {
			$title_active.removeClass('active move')
			$title_new_active.addClass('active')
			$title_active.addClass('move')
			setTimeout(() => {
				$title_content.append(`<span>${$title_active.html()}</span>`)
				$title_active.remove()
				promo__title_animate()
			}, timeStep);

		}, timeMove);
	}

	promo__title_animate()


	onLoad(function(){
		$('.__animate__top').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 0.725

			gsap.to(el, {
				scrollTrigger: {
					trigger: el[0],
					start: 'top 85%',
				},
				y: '0',
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate__bottom').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 0.725

			gsap.to(el, {
				scrollTrigger: {
					trigger: el[0],
					start: 'top 85%',
				},
				y: '0',
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})


		$('.__animate__top__big').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 0.725

			gsap.to(el, {
				scrollTrigger: {
					trigger: el[0],
					start: 'top 85%',
				},
				y: '0',
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate__move__top').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: el[0],
					start: 'top 70%',
				},
				y: '-100%',
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate__scale').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: elParent,
					start: 'top 85%',
				},
				scale: 1,
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate__scale_down').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: elParent,
					start: 'top 85%',
				},
				scale: 1,
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})


		$('.__animate__small_scale').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: elParent,
					start: 'top 85%',
				},
				scale: 1,
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate_home__hero__image__line').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: elParent,
					start: 'top 85%',
				},
				scaleX: 1,
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})



		$('.__animate__left').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: el[0],
					start: 'top 85%',
				},
				x: '0',
				opacity: 1,
				ease: 'ease-in-out',
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate__title').each(function () {
			el = $(this).find('span')
			elParent = $(this).parent()[0]
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1
			let delay = $(this).data('delay') ? $(this).data('delay') : 0

			gsap.to(el, {
				scrollTrigger: {
					trigger: elParent,
					start: 'top 85%',
				},
				y: '0',
				opacity: 1,
				ease: 'ease-in-out',
				stagger: {
					amount: 0.3,
				},
				delay: delay,
				duration: duration,
			})
		})

		$('.__animate__opacity').each(function () {
			el = $(this)
			elParent = $(this).parent()[0]
			let delay = $(this).data('delay') ? $(this).data('delay') : 0
			let duration = $(this).data('duration')
				? $(this).data('duration')
				: 1

			gsap.to(el, {
				scrollTrigger: {
					trigger: el[0],
					start: 'top 85%',
				},
				y: '0',
				opacity: 1,
				ease: 'ease-in-out',
				duration: duration,
				delay: delay,
			})
		})
	})
})
