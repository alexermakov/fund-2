<?php $version = '0.0.5';?>

<!DOCTYPE html>
<html lang="en-EN">

<head>
    <title>Home 2</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png?1">
    <meta name="msapplication-TileColor" content="#876B3E">
    <meta name="theme-color" content="#ffffff">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Koulen&family=Nunito:ital,wght@0,200..1000;1,200..1000&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/vendor/select2.min.css">
    <link rel="stylesheet" href="css/vendor/slick.css">
    <link rel="stylesheet" href="css/vendor/fancybox.css">
    <link rel="stylesheet" href="css/app.min-2.css?<?= $version;?>">
</head>

<body>
    <div class="page__wrap">

        <header class="header_main __animate__bottom">
            <div class="container">

                <a href="index.php" class="header__logo">
                    <img src="images/pages/index-2/logo.svg" alt="main logo">
                </a>

                <div class="header__info">
                    <a href="tel:646-462-6380" class="header__phone">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.44896 10.8478C9.44896 10.8478 10.5737 10.2039 10.8681 10.0492C11.1617 9.89394 11.4659 9.85477 11.6506 9.96755C11.9303 10.1387 14.2792 11.7012 14.477 11.8395C14.6748 11.9781 14.7701 12.374 14.4981 12.7618C14.2273 13.1495 12.9778 14.6829 12.4484 14.6665C11.9181 14.6493 9.7134 14.601 5.55591 10.4423C1.39926 6.28499 1.35009 4.07968 1.33315 3.54939C1.3162 3.01938 2.84953 1.76964 3.23731 1.49853C3.62564 1.22769 4.02203 1.32964 4.15981 1.52019C4.31647 1.73714 5.86091 4.07857 6.03091 4.34691C6.1473 4.53024 6.10452 4.83608 5.94924 5.12997C5.7948 5.42442 5.15091 6.54916 5.15091 6.54916C5.15091 6.54916 5.6048 7.32334 7.1398 8.85809C8.67507 10.3931 9.44896 10.8478 9.44896 10.8478Z" stroke="#1E1E1E" stroke-miterlimit="10" />
                        </svg>
                        <span>646-462-6380</span>
                    </a>
                    <a href="" class="btn_header__info btn_default">Apply now</a>
                </div>

            </div>
        </header>

        <!-- end header -->

        <div class="home__hero">
            <div class="container">

                <div class="home__hero__promo __animate__small_scale __animate__opacity">
                    <img src="images/pages/index-2/bg_promo.svg" alt="promo bg">
                </div>

                <div class="home__hero__info __animate__opacity" data-delay='0.5'>
                    <div class="home__hero__subtitle">Real Estate Investors are welcome!</div>
                    <div class="home__hero__title">
                        <div class="home__hero__title-1">10 times faster</div>
                        <div class="home__hero__title-2">
                            <div class="home__hero__title-2__mask js_home_promo__title__mask__content">
                                <span class='active'>application review</span>
                                <span>approval decision</span>
                                <span>funding process</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="home__hero__form __animate__top__big" data-delay='0.75' data-duration='1.35'>

                    <div class="home__hero__form_content">
                        <div class="home__hero__form_content_title">Fund your project now. </div>
                        <form action="" class="home__hero__form_x js_form">
                            <div class="field_x__group">
                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">First name</div>
                                        <div class="field_x__item_value">
                                            <input type="text" name="firstname" placeholder="First name" required>
                                        </div>
                                    </label>
                                </div>
                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Last name</div>
                                        <div class="field_x__item_value">
                                            <input type="text" name="lastname" placeholder="Last name" required>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Email</div>
                                        <div class="field_x__item_value">
                                            <input type="email" name="email" placeholder="Email" required>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Phone</div>
                                        <div class="field_x__item_value">
                                            <input type="phone" name="phone" placeholder="Phone" required>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <label class="field_x__item">
                                        <div class="field_x__item_title">Profession</div>
                                        <div class="field_x__item_value">
                                            <select name="profession" class='js_select' placeholder='Select'>
                                                <option value="" dissabled></option>
                                                <option value="1">option 1</option>
                                                <option value="2">option 2</option>
                                                <option value="3">option 3</option>
                                            </select>
                                        </div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <label class="field_x__item field_x__item--checkbox">
                                        <input type="checkbox" name="agree" required>
                                        <div class="field_x__item--checkbox__view"></div>
                                        <div class="field_x__item--checkbox__title">Yes, I would like to receive text messaging</div>
                                    </label>
                                </div>

                                <div class="field_x__row">
                                    <div class="field_x__item">
                                        <button class="btn_default btn_x btn_form_hero">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <div class="home__triggers">
            <div class="container">
                <div class="home__triggers__title __animate__top">Your funding problem..Solved</div>
                <div class="home__triggers__list">
                    <div class="home__triggers__item __animate__opacity">
                        <div class="home__triggers__item__icon">
                            <img src="images/pages/index-2/triggers/1.svg">
                        </div>
                        <div class="home__triggers__item__title">Residential & commercial</div>
                        <div class="home__triggers__item__text">Get an offer to finance your real estate project with terms that suit your individual needs.</div>
                    </div>

                    <div class="home__triggers__item __animate__opacity" data-delay='0.2'>
                        <div class="home__triggers__item__icon">
                            <img src="images/pages/index-2/triggers/2.svg">
                        </div>
                        <div class="home__triggers__item__title">Debt or equity</div>
                        <div class="home__triggers__item__text">Structure your deal with repayment options that work for you and your specific requirements.</div>
                    </div>

                    <div class="home__triggers__item __animate__opacity" data-delay='0.4'>
                        <div class="home__triggers__item__icon">
                            <img src="images/pages/index-2/triggers/3.svg">
                        </div>
                        <div class="home__triggers__item__title">No bank</div>
                        <div class="home__triggers__item__text">Don’t get bogged down by all the red tape of banks or mortgage companies. Get funded simply, quickly.</div>
                    </div>

                    <div class="home__triggers__item __animate__opacity" data-delay='0.6'>
                        <div class="home__triggers__item__icon">
                            <img src="images/pages/index-2/triggers/4.svg">
                        </div>
                        <div class="home__triggers__item__title">Fust funding & commercial</div>
                        <div class="home__triggers__item__text">Hard money is fast money. Don’t miss out on that perfect real estate deal. Get the financing you need when you need it.</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="home_sliding js_home_sliding">
            <div class="container">
                <div class="home_sliding__images js_home_sliding__images">
                    <img class='active' src="images/pages/index-2/slider/1.svg" alt="">
                    <img src="images/pages/index-2/slider/2.svg" alt="">
                    <img src="images/pages/index-2/slider/3.svg" alt="">
                </div>
                <div class="home_sliding__list">
                    <div class="home_sliding__slide">
                        <div class="home_sliding__slide__content">
                            <div class="home_sliding__slide__content__top __animate__top">
                                <div class="home_sliding__slide__content__step">
                                    The 10 X FUND light speed Process <span class="__orange">Step 1</span>
                                </div>
                                <div class="home_sliding__slide__content__title">RECEIVE QUOTE today</div>
                            </div>
                            <div class="home_sliding__slide__content__image __animate__opacity">
                                <img src="images/pages/index-2/slider/1.svg" alt="">
                            </div>
                            <div class="home_sliding__slide__content__bottom __animate__top">
                                <div class="home_sliding__slide__content__text">
                                    <p>get a quote from our private lending team <br>in just a few minutes.</p>
                                </div>
                                <a href="" class="btn_default btn_x btn_sliding_slide">Get quote now</a>
                            </div>
                        </div>
                    </div>

                    <div class="home_sliding__slide">
                        <div class="home_sliding__slide__content">
                            <div class="home_sliding__slide__content__top __animate__top">
                                <div class="home_sliding__slide__content__step">
                                    The 10 X FUND light speed Process <span class="__orange">Step 2</span>
                                </div>
                                <div class="home_sliding__slide__content__title">Fast Pre-Qualification </div>
                            </div>
                            <div class="home_sliding__slide__content__image">
                                <img src="images/pages/index-2/slider/2.svg" alt="">
                            </div>
                            <div class="home_sliding__slide__content__bottom __animate__top">
                                <div class="home_sliding__slide__content__text">
                                    <p>Secure your pre-authorization in just a few hours! With our rate lock feature, you can rest assured that you'll get the best possible interest rate. </p>
                                </div>
                                <a href="" class="btn_default btn_x btn_sliding_slide">let’s do business</a>
                            </div>
                        </div>
                    </div>

                    <div class="home_sliding__slide">
                        <div class="home_sliding__slide__content">
                            <div class="home_sliding__slide__content__top __animate__top">
                                <div class="home_sliding__slide__content__step">
                                    The 10 X FUND light speed Process <span class="__orange">Step 3</span>
                                </div>
                                <div class="home_sliding__slide__content__title">Finalize Loan and fund your project.</div>
                            </div>
                            <div class="home_sliding__slide__content__image __animate__opacity">
                                <img src="images/pages/index-2/slider/3.svg" alt="">
                            </div>
                            <div class="home_sliding__slide__content__bottom __animate__top">
                                <div class="home_sliding__slide__content__text">
                                    <p>After receiving all the necessary information, we will process the appraisal and title requirements. Once the title and appraisal are clear, we will close the deal within five business days.</p>
                                </div>
                                <a href="" class="btn_default btn_x btn_sliding_slide">fund your project</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="home_question">
            <div class="home_question__top">
                <div class="container">
                    <div class="home_question__subtitle __animate__top">F.A.Q.</div>
                    <div class="home_question__title __animate__top">Frequently Asked Questions</div>
                </div>
            </div>
            <div class="home_question__list">
                <?php $data =
                        array(
                            array(
                                'title'=>"How soon I'll get my quote?",
                                'text'=>"In most cases, our experienced loan managers can provide you with a quote for your loan within minutes.",
                            ),
                            array(
                                'title'=>"How long is your pre-qualification process? ",
                                'text'=>"It is based on a case-by-case, but wouldn't take moree than few days.
                                We require financial documents for assessment and review. Upon verification, you will be pre-authorized if everything is in order.",
                            ),
                            array(
                                'title'=>"What type of loans are available?",
                                'text'=>"We provide financing solutions for real estate investors, including fix-and-flip, rental, and stabilized bridge loans, tailored to help achieve their investment goals.",
                            ),
                        );
                    ?>
                <?php foreach ($data as $key => $item):?>
                <div class="home_question__item js_home_question__item __animate__top">
                    <div class="container">
                        <div class="home_question__item__top">
                            <div class="home_question__item__title"><?= $item['title'];?></div>
                            <div class="home_question__item__btn">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L14.2325 14.2325" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M14.9999 1L14.9999 14.9999L1 15" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <div class="home_question__item__content">
                            <div class="home_question__item__text"><?= $item['text'];?></div>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>


        <div class="home__counts">
            <div class="container">
                <div class="home__counts__info __animate__top">
                    <div class="home__counts__info__text">
                        <p>Сconcentrates on real estate investments that feature asymmetrical risk-reward on all asset classes throughout the United States.</p>
                    </div>
                    <a href="" class="btn_default btn_x btn_home__counts">partner up with the best</a>
                </div>
                <div class="home__counts__list">
                    <div class="home__counts__item __animate__top">
                        <div class="home__counts__item__value"><span class='js_count_animate' data-number="100">0</span>+</div>
                        <div class="home__counts__item__text">Real Estate Investments</div>
                    </div>
                    <div class="home__counts__item __animate__top" data-delay='0.15'>
                        <div class="home__counts__item__value"><span class='js_count_animate' data-number="70">0</span>+</div>
                        <div class="home__counts__item__text">Properties Under Management</div>
                    </div>
                    <div class="home__counts__item __animate__top" data-delay='0.3'>
                        <div class="home__counts__item__value"><span class='js_count_animate' data-number="10000">0</span>+</div>
                        <div class="home__counts__item__text">Multifamily Units</div>
                    </div>
                    <div class="home__counts__item __animate__top" data-delay='0.45'>
                        <div class="home__counts__item__value"><span class='js_count_animate' data-number="25">0</span>+</div>
                        <div class="home__counts__item__text">U.S. States</div>
                    </div>
                </div>
            </div>
        </div>



        <div class="portfolio_home">
            <div class="container">
                <div class="portfolio_home__info __animate__opacity">
                    <div class="portfolio_home__subtitle __animate__top" data-delay='0.1'>Portfolio</div>
                    <div class="portfolio_home__title __animate__top" data-delay='0.1'>Why look elsewhere for funding?</div>
                    <div class="portfolio_home__text __animate__top" data-delay='0.1'>
                        <p>Requesting funding costs nothing. As soon as you make your request, our expert team will go to work. We pride ourselves on offering a range of lending options, and truly believe we can provide you with the funding you need, at reasonable rates. You have nothing to lose – and a whole lot to gain.</p>
                    </div>
                    <a href="" class="btn_default btn_x btn_portfolio_home">Get a same day commitment</a>
                </div>
                <div class="portfolio_home__items">
                    <div class="portfolio_home__list">
                        <?php
                                $data = array(
                                    array(
                                        'title'  => 'Cleveland, OH',
                                        'text_1' => 'Flix and Flip',
                                        'text_2' => '85% LTC',
                                        'image'  => 'images/__content/pages/home/object/1.jpg',
                                    ),
                                    array(
                                        'title'  => 'Cleveland, OH',
                                        'text_1' => 'Flix and Flip',
                                        'text_2' => '85% LTC',
                                        'image'  => 'images/__content/pages/home/object/2.jpg',
                                    ),
                                    array(
                                        'title'  => 'Cleveland, OH',
                                        'text_1' => 'Flix and Flip',
                                        'text_2' => '85% LTC',
                                        'image'  => 'images/__content/pages/home/object/3.jpg',
                                    ),
                                    array(
                                        'title'  => 'Cleveland, OH',
                                        'text_1' => 'Flix and Flip',
                                        'text_2' => '85% LTC',
                                        'image'  => 'images/__content/pages/home/object/4.jpg',
                                    ),
                                );
                            ?>
                        <?php foreach ($data as $key => $item):?>
                        <div class="home_object__item __animate__opacity" data-delay="<?= $key * 0.1;?>">
                            <div class="home_object__item__image">
                                <img class='__animate__scale_down' data-duration='1' data-delay='<?= $key * 0.1;?>' src="<?= $item['image'];?>">
                            </div>
                            <div class="home_object__item__info">
                                <div class="home_object__item__title">
                                    <a href="" class="home_object__item__link">
                                        <span><?= $item['title'];?></span>
                                        <svg width="18" height="8" viewBox="0 0 18 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 3.5C0.723858 3.5 0.5 3.72386 0.5 4C0.5 4.27614 0.723858 4.5 1 4.5V3.5ZM17.3536 4.35355C17.5488 4.15829 17.5488 3.84171 17.3536 3.64645L14.1716 0.464466C13.9763 0.269204 13.6597 0.269204 13.4645 0.464466C13.2692 0.659728 13.2692 0.976311 13.4645 1.17157L16.2929 4L13.4645 6.82843C13.2692 7.02369 13.2692 7.34027 13.4645 7.53553C13.6597 7.7308 13.9763 7.7308 14.1716 7.53553L17.3536 4.35355ZM1 4.5H17V3.5H1V4.5Z" fill="#716A56" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="home_object__item__text_1"><?= $item['text_1'];?></div>
                                <div class="home_object__item__text_2"><?= $item['text_2'];?></div>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>


        <!-- start footer -->

        <a href="tel:646-462-6380" class="fixed__call js_fixed__call">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.4167 17.125V20.25C22.4179 20.5401 22.3585 20.8273 22.2423 21.0931C22.126 21.3589 21.9556 21.5975 21.7418 21.7936C21.528 21.9898 21.2757 22.1391 21.0009 22.232C20.726 22.3249 20.4348 22.3595 20.1459 22.3334C16.9405 21.9851 13.8615 20.8898 11.1563 19.1354C8.63948 17.5361 6.50563 15.4023 4.90632 12.8854C3.14588 10.1679 2.05033 7.07397 1.70841 3.85419C1.68238 3.56613 1.71661 3.27581 1.80893 3.00171C1.90125 2.72761 2.04963 2.47574 2.24462 2.26212C2.43962 2.04851 2.67695 1.87784 2.94152 1.76098C3.20609 1.64412 3.49209 1.58363 3.78132 1.58335H6.90632C7.41185 1.57838 7.90194 1.75739 8.28524 2.08703C8.66854 2.41667 8.9189 2.87444 8.98966 3.37502C9.12155 4.37509 9.36617 5.35703 9.71882 6.3021C9.85897 6.67494 9.8893 7.08014 9.80623 7.46969C9.72315 7.85923 9.53014 8.2168 9.25007 8.50002L7.92716 9.82294C9.41003 12.4308 11.5693 14.5901 14.1772 16.0729L15.5001 14.75C15.7833 14.47 16.1409 14.2769 16.5304 14.1939C16.92 14.1108 17.3252 14.1411 17.698 14.2813C18.6431 14.6339 19.625 14.8785 20.6251 15.0104C21.1311 15.0818 21.5932 15.3367 21.9235 15.7266C22.2539 16.1165 22.4294 16.6142 22.4167 17.125Z" stroke="white" stroke-width="2.08333" stroke-linecap="round" stroke-linejoin="round" />
            </svg>
        </a>


        <footer class="main_footer">
            <div class="footer__top">
                <div class="container">
                    <div class="footer__top__content">
                        <div class="footer__top__title __animate__top">
                            NEW YORK OFFICE <br>
                            9 Park Place, 1st Floor <br>
                            Greatr Neck Plaza, NY 11021
                        </div>

                        <div class="footer__top__contact">
                            <div class="footer__top__contact_list">
                                <div class="footer__top__contact_item __animate__top">
                                    <div class="footer__top__contact_item__title">Phone</div>
                                    <div class="footer__top__contact_item__value"><a href="tel:+1 (347) 263-3592">+1 (347) 263-3592</a></div>
                                </div>
                                <div class="footer__top__contact_item __animate__top">
                                    <div class="footer__top__contact_item__title">Email</div>
                                    <div class="footer__top__contact_item__value"><a href="mailto:info@lendvent.com">info@lendvent.com</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer__top__form">
                        <div class="footer_form__title __animate__top">We are here to help</div>
                        <div class="footer_form__content __animate__opacity">
                            <form action="" class="js_form">
                                <div class="field_x__group">
                                    <div class="field_x__row">
                                        <label class="field_x__item">
                                            <div class="field_x__item_title">Your name</div>
                                            <div class="field_x__item_value">
                                                <input type="text" name="fullname" placeholder="Full name" required>
                                            </div>
                                        </label>
                                        <label class="field_x__item">
                                            <div class="field_x__item_title">Company name</div>
                                            <div class="field_x__item_value">
                                                <input type="text" name="companyname" placeholder="Company name" required>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="field_x__row">
                                        <label class="field_x__item">
                                            <div class="field_x__item_title">Email</div>
                                            <div class="field_x__item_value">
                                                <input type="email" name="email" placeholder="Email" required>
                                            </div>
                                        </label>
                                        <label class="field_x__item">
                                            <div class="field_x__item_title">Phone</div>
                                            <div class="field_x__item_value">
                                                <input type="phone" name="phone" placeholder="Phone" required>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="field_x__row">
                                        <label class="field_x__item">
                                            <div class="field_x__item_title">Message</div>
                                            <div class="field_x__item_value">
                                                <textarea name="message" placeholder="Enter here"></textarea>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="field_x__row">
                                        <div class="field_x__item">
                                            <button class="btn_default btn_x btn_form_footer">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer__bottom __animate__top">
                <div class="container">
                    <div class="footer__social">
                        <a href="" class="footer__social__link" target='_blank'>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M17 2H7C4.23858 2 2 4.23858 2 7V17C2 19.7614 4.23858 22 7 22H17C19.7614 22 22 19.7614 22 17V7C22 4.23858 19.7614 2 17 2Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M15.9997 11.3698C16.1231 12.2021 15.981 13.052 15.5935 13.7988C15.206 14.5456 14.5929 15.1512 13.8413 15.5295C13.0898 15.9077 12.2382 16.0394 11.4075 15.9057C10.5768 15.7721 9.80947 15.3799 9.21455 14.785C8.61962 14.1901 8.22744 13.4227 8.09377 12.592C7.96011 11.7614 8.09177 10.9097 8.47003 10.1582C8.84829 9.40667 9.45389 8.79355 10.2007 8.40605C10.9475 8.01856 11.7975 7.8764 12.6297 7.99981C13.4786 8.1257 14.2646 8.52128 14.8714 9.12812C15.4782 9.73496 15.8738 10.5209 15.9997 11.3698Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M17.5 6.5H17.51" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </a>
                        <a href="" class="footer__social__link" target='_blank'>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18 2H15C13.6739 2 12.4021 2.52678 11.4645 3.46447C10.5268 4.40215 10 5.67392 10 7V10H7V14H10V22H14V14H17L18 10H14V7C14 6.73478 14.1054 6.48043 14.2929 6.29289C14.4804 6.10536 14.7348 6 15 6H18V2Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </a>
                    </div>
                    <div class="footer__bottom__info">
                        <div class="footer__bottom__info__inner">
                            <div class="footer__bottom__logo"><img src="images/pages/index-2/logo-footer.svg" alt=""></div>
                            <div class="footer__bottom__copy">© 2024 All rights reserved</div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>



        <!-- modal -->




        <div class="modal_default modal_call" id='js_modal_call'>
            <div class="modal_default__inner">
                <div class="modal_default__title">Request call</div>
                <div class="modal_default__form">
                    <form action="javascript:void(0)" class="js_form">
                        <div class="field_x__group">
                            <div class="field_x__row">
                                <label class="field_x__item">
                                    <div class="field_x__item_title">Name</div>
                                    <div class="field_x__item_value">
                                        <input type="text" name="name" placeholder="Name" required>
                                    </div>
                                </label>
                            </div>

                            <div class="field_x__row">
                                <label class="field_x__item">
                                    <div class="field_x__item_title">Phone</div>
                                    <div class="field_x__item_value">
                                        <input type="phone" name="phone" placeholder="Phone" required>
                                    </div>
                                </label>
                            </div>

                            <div class="field_x__row">
                                <div class="field_x__item field_x__item--radio">
                                    <div class="field_x__item_title">Select department</div>
                                    <div class="field_x__item_value field_x__item_value--radio">
                                        <label class="field_x__item_value--radio_item">
                                            <input type='radio' name='department' required>
                                            <div class="field_x__item_value--radio_item__view"></div>
                                            <div class="field_x__item_value--radio_item__title">Sales</div>
                                        </label>
                                        <label class="field_x__item_value--radio_item">
                                            <input type='radio' name='department' required>
                                            <div class="field_x__item_value--radio_item__view"></div>
                                            <div class="field_x__item_value--radio_item__title">Customer Service</div>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="field_x__row">
                                <div class="field_x__item">
                                    <button class="btn_default btn_x btn_form_modal_form js_btn_form_modal_form">Request call</button>
                                </div>
                            </div>
                            <div class="field_x__row">
                                <div class="field_x__row__add_text">Customer service representative will call you in 2-3 minutes</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal_default modal_thanks" id='js_modal_thanks'>
            <div class="modal_default__inner modal_default__inner--thanks">
                <div class="modal_thanks__icon">
                    <img src="images/icons/social/thanks.svg" alt="thanks">
                </div>
                <div class="modal_default__title modal_default__title--thanks">Thank you!</div>
                <div class="modal_default__text modal_default__text--thanks">We've got your request and will call you in a few minutes.</div>
            </div>
        </div>


        <script src="js/vendor/jquery.min.js"></script>
        <script src="js/vendor/fancybox.umd.js"></script>
        <script src="js/vendor/slick.min.js"></script>
        <script src="js/vendor/gsap.min.js"></script>
        <script src="js/vendor/ScrollTrigger.min.js"></script>
        <script src="js/vendor/jquery.inputmask.bundle.js"></script>
        <script src="js/vendor/select2.min.js"></script>
        <script src="js/vendor/stickyfill.min.js"></script>
        <script src="js/main-2.js?<?= $version;?>"></script>
        <script src="js/animate.js?<?= $version;?>"></script>


    </div>
</body>

</html>